import { IhomeAbout } from './../../models/IhomeAbout';
import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-home-about',
  templateUrl: './home-about.component.html',
  styleUrls: ['./home-about.component.scss']
})
export class HomeAboutComponent implements OnInit {
  @Input() public dataAbout!: IhomeAbout[];

  constructor() { }

  ngOnInit(): void {
  }

}
