
import { HomeService } from './../../home-services/home.service';
import { IhomeMain } from './../../models/IhomeMain';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-main',
  templateUrl: './home-main.component.html',
  styleUrls: ['./home-main.component.scss']
})
export class HomeMainComponent implements OnInit {
  @Input() public dataMain!: IhomeMain[];


  constructor() { }

  ngOnInit(): void {

  }


}
