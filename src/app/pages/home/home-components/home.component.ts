import { HomeService } from './../home-services/home.service';
import { IhomeRrss } from './../models/IhomeRrss';
import { Component, OnInit } from '@angular/core';
import { IhomeAbout } from '../models/IhomeAbout';
import { IhomeMain } from '../models/IhomeMain';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public dataHomeMain!: IhomeMain[];
  public dataHomeAbout!: IhomeAbout[];
  public dataHomeRrss!: IhomeRrss[];

  constructor( private homeService: HomeService) { }

  ngOnInit(): void {
    this.homeMainData();
    this.homeAboutData();
    this.homeRrssData();
  }
  public homeMainData(): void {
    this.homeService.getHome().subscribe(
      (dataMain: IhomeMain[]) => {
        this.dataHomeMain = dataMain;
        console.log(dataMain);
      }
    );
  }
  public homeAboutData(): void {
    this.homeService.getHomeAbout().subscribe(
      (dataAbout: IhomeAbout[]) => {
        this.dataHomeAbout = dataAbout;
        console.log(dataAbout);
      }

    );
  }
  public homeRrssData(): void {
    this.homeService.getHomeRrss().subscribe(
      (dataRrss: IhomeRrss[]) => {
        this.dataHomeRrss = dataRrss;
        console.log(dataRrss);
      }

    );
  }

}
