import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRrssComponent } from './home-rrss.component';

describe('HomeRrssComponent', () => {
  let component: HomeRrssComponent;
  let fixture: ComponentFixture<HomeRrssComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeRrssComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRrssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
