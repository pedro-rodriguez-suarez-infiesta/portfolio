import { Component, OnInit, Input } from '@angular/core';
import { IhomeRrss } from '../../models/IhomeRrss';

@Component({
  selector: 'app-home-rrss',
  templateUrl: './home-rrss.component.html',
  styleUrls: ['./home-rrss.component.scss'],
})
export class HomeRrssComponent implements OnInit {
   @Input() public dataRrss!: IhomeRrss[];
  constructor() {}

  ngOnInit(): void {}
}
