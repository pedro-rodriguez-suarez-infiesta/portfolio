export interface IhomeAbout {
    homeAboutTitle?: string;
    homeAboutDescription?: string;
    imageUrl: string;
    imageDescription: string;
    profileImage?: string;

}

/* export interface IprofileImages {
    imageTitle: string;
    imageDescription: string;
    imageUrl: string;
} */
