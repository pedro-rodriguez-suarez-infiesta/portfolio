import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

import { Icontact, Form } from './../models/Icontact';
import { ENDPOINTS } from '../../../../endpoints/endpoints';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private formUrl: string = ENDPOINTS.contactMainUrl;
  private postUserUrl: string = ENDPOINTS.postUserUrl;

  constructor(private http: HttpClient) {
    // Empty
  }
  public getFormStaticData(): Observable<Icontact[]> {
    return this.http.get(this.formUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
  public postUser(user: Form): Observable<any> {
    return this.http.post(this.postUserUrl, user).pipe(
      map((response: any) => {
      if (!response) {
        throw new Error('Value expected');
      } else {
        return response;
      }
    }),
    catchError((err) => {
      throw new Error(err.message);
    })
    );
}
}
