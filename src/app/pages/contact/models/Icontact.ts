export interface Icontact {
    id: number;
    name: string;
    formDescription: string;
    imageUrl: string;

}



export interface Form {
    name: string;
    surname: string;
    email: string;
    message: string;
    subject: string;
}

