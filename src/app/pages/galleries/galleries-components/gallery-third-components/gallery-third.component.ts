import { IgalleryThird } from './../../models/IgalleryThird';
import { Component, Input, OnInit } from '@angular/core';
import { GalleryThirdService } from './gallery-thid-services/gallery-third.service';

@Component({
  selector: 'app-gallery-third',
  templateUrl: './gallery-third.component.html',
  styleUrls: ['./gallery-third.component.scss'],
})
export class GalleryThirdComponent implements OnInit {
  public dataGalleryThird!: IgalleryThird[];
  constructor(private galleryThirdService: GalleryThirdService) {}

  ngOnInit(): void {
    this.galleryThirdData();
    console.log(this.dataGalleryThird);
  }
  public galleryThirdData(): void {
    this.galleryThirdService
      .getGalleryThirdData()
      .subscribe((data: IgalleryThird[]) => {
        console.log(data);
        this.dataGalleryThird = data;
      });
  }
}
