import { IgalleryThird } from './../../../models/IgalleryThird';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ENDPOINTS } from 'src/endpoints/endpoints';


@Injectable({
  providedIn: 'root'
})
export class GalleryThirdService {
  private galleryThirdUrl: string = ENDPOINTS.galleryThirdUrl;

  constructor(private http: HttpClient) {
    // Empty
  }
  public getGalleryThirdData(): Observable<IgalleryThird[]> {
    return this.http.get(this.galleryThirdUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
