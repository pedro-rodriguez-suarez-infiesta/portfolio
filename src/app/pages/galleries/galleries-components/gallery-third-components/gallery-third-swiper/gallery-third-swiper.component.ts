import { ISwiper } from '../../../models/ISwiper';
import { GalleryThirdService } from '../gallery-thid-services/gallery-third.service';
import { IgalleryThird } from '../../../models/IgalleryThird';
import { Component, Input, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';


import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';





@Component({
  selector: 'app-gallery-third-swiper',
  templateUrl: './gallery-third-swiper.component.html',
  styleUrls: ['./gallery-third-swiper.component.scss']
})
export class GalleryThirdSwiperComponent implements OnInit {
  @Input()
  dataGalleryThird!: ISwiper[] | any ;



  public config: SwiperConfigInterface = {
    direction: 'horizontal',
    threshold: 50,
    spaceBetween: 5,
    slidesPerView: 1,
    centeredSlides: true,
    keyboard: true,
    zoom: {
      maxRatio: 5,
    },
    mousewheel: {
      invert: true,
    },
    fadeEffect: {
      crossFade: true,
    },
    scrollbar: true,
    autoplay: {
      delay: 4000,
      stopOnLastSlide: false,
      reverseDirection: false,
      disableOnInteraction: false,
    }
  };

  constructor() { }

  ngOnInit(): void {
  }

}

