import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryThirdSwiperComponent } from './gallery-third-swiper.component';

describe('GalleryThirdSwiperComponent', () => {
  let component: GalleryThirdSwiperComponent;
  let fixture: ComponentFixture<GalleryThirdSwiperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GalleryThirdSwiperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryThirdSwiperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
