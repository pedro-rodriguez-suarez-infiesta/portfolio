import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ENDPOINTS } from 'src/endpoints/endpoints';
import { IgallerySecond } from '../../../models/IgallerySecond';

@Injectable({
  providedIn: 'root'
})
export class GallerySecondService {
  private gallerySecondUrl: string = ENDPOINTS.gallerySecondUrl;

  constructor(private http: HttpClient) {
    // Empty
  }
  public getGallerySecondData(): Observable<IgallerySecond[]> {
    return this.http.get(this.gallerySecondUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
