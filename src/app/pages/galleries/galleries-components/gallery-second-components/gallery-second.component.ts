
import { IgallerySecond } from './../../models/IgallerySecond';
import { Component, Input, OnInit } from '@angular/core';
import { GallerySecondService } from './gallery-second-services/gallery-second.service';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

const SwiperConfigInterface = {
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: true,
  
};
@Component({
  selector: 'app-gallery-second',
  templateUrl: './gallery-second.component.html',
  styleUrls: ['./gallery-second.component.scss']
})
export class GallerySecondComponent implements OnInit {
  public dataGallerySecond!: IgallerySecond[];

  constructor(private gallerySecondService: GallerySecondService) { }

  ngOnInit(): void {
    this.gallerySecondData();
    console.log(this.dataGallerySecond);
  }
  public gallerySecondData(): void {
    this.gallerySecondService.getGallerySecondData().subscribe(
      (data: IgallerySecond[]) => {
        console.log(data);
        this.dataGallerySecond = data;

      }
    );

  }


}
