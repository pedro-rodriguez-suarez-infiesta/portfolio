import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GallerySecondSwiperComponent } from './gallery-second-swiper.component';

describe('GallerySecondSwiperComponent', () => {
  let component: GallerySecondSwiperComponent;
  let fixture: ComponentFixture<GallerySecondSwiperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GallerySecondSwiperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GallerySecondSwiperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
