import { ISwiper } from './../../../../models/ISwiper';
import { GallerySecondService } from './../../gallery-second-services/gallery-second.service';
import { IgallerySecond } from './../../../../models/IgallerySecond';
import { Component, Input, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';


import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';





@Component({
  selector: 'app-gallery-second-swiper',
  templateUrl: './gallery-second-swiper.component.html',
  styleUrls: ['./gallery-second-swiper.component.scss']
})
export class GallerySecondSwiperComponent implements OnInit {
  @Input()
  dataGallerySecond!: ISwiper[] | any ;



  public config: SwiperConfigInterface = {
    direction: 'horizontal',
    threshold: 50,
    spaceBetween: 5,
    slidesPerView: 1,
    centeredSlides: true,
    keyboard: true,
    zoom: {
      maxRatio: 5,
    },
    mousewheel: {
      invert: true,
    },
    fadeEffect: {
      crossFade: true,
    },
    scrollbar: true,
    autoplay: {
      delay: 4000,
      stopOnLastSlide: false,
      reverseDirection: false,
      disableOnInteraction: false,
    }
  };

  constructor() { }

  ngOnInit(): void {
  }

}

