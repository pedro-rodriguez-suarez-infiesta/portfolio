import { GalleryFourthService } from './gallery-fourth-services/gallery-fourth.service';
import { IgalleryFourth } from './../../models/IgalleryFourth';
import { Component, Input, OnInit } from '@angular/core';
import Swiper from 'swiper/bundle';
import 'swiper/swiper-bundle.css';


@Component({
  selector: 'app-gallery-fourth',
  templateUrl: './gallery-fourth.component.html',
  styleUrls: ['./gallery-fourth.component.scss']
})
export class GalleryFourthComponent implements OnInit {
 public dataGalleryFourth!: IgalleryFourth[];


  constructor(private galleryFourthService: GalleryFourthService) { }

  ngOnInit(): void {
    this.galleryFourthData();
    console.log(this.dataGalleryFourth);  }
  public galleryFourthData(): void {
    this.galleryFourthService.getGalleryFourthData().subscribe(
          (data: IgalleryFourth[]) => {
            console.log(data);
            this.dataGalleryFourth = data;
    }
  );

}

}
