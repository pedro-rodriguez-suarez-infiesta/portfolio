import { IgalleryFourth } from './../../../models/IgalleryFourth';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ENDPOINTS } from 'src/endpoints/endpoints';


@Injectable({
  providedIn: 'root'
})
export class GalleryFourthService {
  private galleryFourthUrl: string = ENDPOINTS.galleryFourthUrl;

  constructor(private http: HttpClient) {
    // Empty
  }
  public getGalleryFourthData(): Observable<IgalleryFourth[]> {
    return this.http.get(this.galleryFourthUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
