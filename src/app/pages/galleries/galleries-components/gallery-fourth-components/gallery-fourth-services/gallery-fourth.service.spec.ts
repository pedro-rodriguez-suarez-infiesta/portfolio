import { TestBed } from '@angular/core/testing';

import { GalleryFourthService } from './gallery-fourth.service';

describe('GalleryFourthService', () => {
  let service: GalleryFourthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GalleryFourthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
