import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryFourthSwiperComponent } from './gallery-fourth-swiper.component';

describe('GalleryFourthSwiperComponent', () => {
  let component: GalleryFourthSwiperComponent;
  let fixture: ComponentFixture<GalleryFourthSwiperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GalleryFourthSwiperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryFourthSwiperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
