import { IgalleryFourth } from './../../../models/IgalleryFourth';
import { ISwiper } from '../../../models/ISwiper';
import { Component, Input, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';


import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';





@Component({
  selector: 'app-gallery-fourth-swiper',
  templateUrl: './gallery-fourth-swiper.component.html',
  styleUrls: ['./gallery-fourth-swiper.component.scss']
})
export class GalleryFourthSwiperComponent implements OnInit {
  @Input()
  dataGalleryFourth!: ISwiper[] | any ;



  public config: SwiperConfigInterface = {
    direction: 'horizontal',
    threshold: 50,
    spaceBetween: 5,
    slidesPerView: 1,
    centeredSlides: true,
    keyboard: true,
    zoom: {
      maxRatio: 5,
    },
    mousewheel: {
      invert: true,
    },
    fadeEffect: {
      crossFade: true,
    },
    scrollbar: true,
    autoplay: {
      delay: 4000,
      stopOnLastSlide: false,
      reverseDirection: false,
      disableOnInteraction: false,
    }
  };

  constructor() { }

  ngOnInit(): void {
  }

}

