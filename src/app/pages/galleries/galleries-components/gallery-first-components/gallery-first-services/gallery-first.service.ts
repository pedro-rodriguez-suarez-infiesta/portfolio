import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ENDPOINTS } from 'src/endpoints/endpoints';
import { IgalleryFirst } from '../../../models/IgalleryFirst';

@Injectable({
  providedIn: 'root',
})
export class GalleryFirstService {
  private galleryFirstUrl: string = ENDPOINTS.galleryFirstUrl;

  constructor(private http: HttpClient) {
    // Empty
  }
  public getGalleryFirstData(): Observable<IgalleryFirst[]> {
    return this.http.get(this.galleryFirstUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
