import { IgalleryFirst } from './../../models/IgalleryFirst';

import { GalleryFirstService } from '../gallery-first-components/gallery-first-services/gallery-first.service';

import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-gallery',
  templateUrl: './gallery-first.component.html',
  styleUrls: ['./gallery-first.component.scss']
})
export class GalleryFirstComponent implements OnInit {
  public dataGalleryFirst!: IgalleryFirst[];

  constructor(private galleryFirstService: GalleryFirstService) {
    // Empty
  }

  ngOnInit(): void {
    this.galleryFirstData();
    console.log(this.dataGalleryFirst);
  }
  public galleryFirstData(): void {
    this.galleryFirstService.getGalleryFirstData().subscribe(
      (data: IgalleryFirst[]) => {
        console.log(data);
        this.dataGalleryFirst = data;
      }
    );

  }


}

