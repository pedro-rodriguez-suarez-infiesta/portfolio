import { IgalleryVideo } from './../../models/IgalleryVideo';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  @Input() public dataGalleryVideo!: IgalleryVideo[];

  constructor() { }

  ngOnInit(): void {
  }

}
