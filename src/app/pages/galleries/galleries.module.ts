
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { GalleriesRoutingModule } from './galleries-routing.module';
import { GalleriesComponent } from './galleries-components/galleries.component';
import { GalleryFirstComponent } from './galleries-components/gallery-first-components/gallery-first.component';
import { GallerySecondComponent } from './galleries-components/gallery-second-components/gallery-second.component';
import { GalleryFourthComponent } from './galleries-components/gallery-fourth-components/gallery-fourth.component';
import { GalleryThirdComponent } from './galleries-components/gallery-third-components/gallery-third.component';
import { VideoComponent } from './galleries-components/gallery-video-components/video.component';


import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { GallerySecondSwiperComponent } from './galleries-components/gallery-second-components/gallery-second-swiper/gallery-second-swiper/gallery-second-swiper.component';
import { GalleryThirdSwiperComponent } from './galleries-components/gallery-third-components/gallery-third-swiper/gallery-third-swiper.component';
import { GalleryFourthSwiperComponent } from './galleries-components/gallery-fourth-components/gallery-fourth-swiper/gallery-fourth-swiper.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: true,
};

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [GalleriesComponent, GalleryFirstComponent, GallerySecondComponent, GalleryThirdComponent, GalleryFourthComponent, VideoComponent, GallerySecondSwiperComponent, GalleryThirdSwiperComponent, GalleryFourthSwiperComponent],
  imports: [
    CommonModule,
    GalleriesRoutingModule,
    SwiperModule
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG,
    }
  ]
})
export class GalleriesModule { }
