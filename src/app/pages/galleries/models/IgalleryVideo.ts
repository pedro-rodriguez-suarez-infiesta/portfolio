export interface IgalleryVideo{
    galleryTitle?: string;
    galleryDescription?: string;
    videoTitle: string;
    videoDescription: string;
    videoUrl: string;
}
