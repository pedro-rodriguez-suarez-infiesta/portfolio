export interface Igalleries {
    galleriesInfo: string;
    galleriesDescription: string;
    imageTitle: string;
    imageDescription: string;
    imageUrl: string;
}

