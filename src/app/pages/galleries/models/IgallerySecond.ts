export interface IgallerySecond{
    galleryTitle?: string;
    galleryDescription?: string;
    imageTitle: string;
    imageDescription: string;
    imageUrl: string;
}
