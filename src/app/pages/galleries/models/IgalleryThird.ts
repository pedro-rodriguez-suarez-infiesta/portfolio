export interface IgalleryThird{
    galleryTitle?: string;
    galleryDescription?: string;
    imageTitle: string;
    imageDescription: string;
    imageUrl: string;
}
