export interface IgalleryFourth{
    galleryTitle?: string;
    galleryDescription?: string;
    imageTitle: string;
    imageDescription: string;
    imageUrl: string;
}
